import React,{Component} from 'react';
import './App.css';
import Persons from './containers/Persons';

class App extends Component {
  render(){
    return(
      <div className="App">
        {/* <ol>
          <li></li>
        </ol> */}
        <Persons />
      </div>
    )
  }
 
}

export default App;
